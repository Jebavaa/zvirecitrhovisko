﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace zvireci_trhovisko.Controllers
{
    public static class ConnectionManager
    {

        public static IDbConnection GetConnection()
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["orcl"].ConnectionString;

            var conn = new OracleConnection(connString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            return conn;
        }

        public static void CloseConnection(IDbConnection conn)
        {
            if (conn.State == ConnectionState.Open || conn.State == ConnectionState.Broken)
            {
                conn.Close();
            }
        }

    }
}