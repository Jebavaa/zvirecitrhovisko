﻿using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using zvireci_trhovisko.Utility;
using zvireci_trhovisko.Models;
using Dapper;

namespace zvireci_trhovisko.Controllers
{
    public class HomeController : Controller
    {
        //string conString = "User ID=DBjebavna1; Password=DBjebavna122; Data Source=orcl;";
        string conString = System.Configuration.ConfigurationManager.ConnectionStrings["orcl"].ConnectionString;


        public ActionResult Index()
        {

            List<Uzivatel> result = new List<Uzivatel>();

            string sql = "SELECT * FROM UZIVATEL WHERE UZIVATELSKE_JMENO = :uz_jm AND HESLO = :hsl";
            string uz_jm = "kokos";
            string hsl = "heslo";
            string uzivatelDemo = "";

            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = conString;

                OracleCommand command = new OracleCommand(sql, connection);
                command.Parameters.Add(new OracleParameter("uz_jm", OracleDbType.Varchar2));
                command.Parameters.Add(new OracleParameter("hsl", OracleDbType.Varchar2));
                command.CommandType = CommandType.Text;
                command.Connection = connection;
                command.CommandText = sql;
                command.BindByName = true;

                command.Parameters[0].Value = uz_jm;
                command.Parameters[1].Value = hsl;


                connection.Open();
                
                OracleDataReader reader = command.ExecuteReader();



                /*
                try
                {
                    while (reader.Read()) 
                    {
                        reader.Read();
                        string value = reader["Uzivatelske_jmeno"].ToString();

                        if (value == "kokos")
                        {
                            uzivatelDemo = reader["Uzivatelske_jmeno"].ToString();
                        }
                    }
                }
                catch
                {
                    reader.Close();
                }

                /*
                var param = new DynamicParameters();
                param.Add(":uz_jm", uz_jm);
                param.Add("hsl", hsl);

                var uzivatel = connection.Query<Uzivatel>("SELECT * FROM UZIVATEL WHERE UZIVATELSKE_JMENO = :uz_jm AND HESLO = :hsl", param)
                    .FirstOrDefault();

                /*
                OracleCommand command = new OracleCommand(sql, connection);

                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        uzivatel = reader.GetString(0);
                    }
                }
                */
                connection.Close();
                connection.Dispose();
            }


            ViewBag.Uzivatel = uzivatelDemo;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {

            string ulice = String.Empty;



            ViewBag.Message = ulice;


            return View();
        }

        [HttpGet]
        [Route("GetUzivatelList")]
        public object GetUzivatelList()
        {
            return QueryBuilder<Uzivatel>.GetList(new Uzivatel());
        }

    }
}