﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using zvireci_trhovisko.Models;
using System.Windows.Forms;
using System.Drawing;

namespace zvireci_trhovisko.Controllers
{
    public class ZvireController : Controller
    {

        string conString = System.Configuration.ConfigurationManager.ConnectionStrings["orcl"].ConnectionString;



        public ActionResult Index()
        {
            var listZvirat = new List<Models.Zvire>();

            string sql = "SELECT * FROM zvire";


            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = conString;
                connection.Open();

                OracleCommand command = new OracleCommand(sql, connection);

                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        
                        Zvire zvire = new Zvire();
                        zvire.Id = reader.GetInt32(0);
                        zvire.Id_chovatel = reader.GetInt32(1);
                        zvire.Id_rozmer = reader.GetInt32(2);
                        zvire.Id_druh_zvirete = reader.GetInt32(3);
                        zvire.Id_obrazek = reader.GetInt32(4);
                        zvire.Nazev = reader.GetString(5);
                        zvire.Cena = reader.GetInt32(6);
                        if (reader.GetString(7) != null)
                        {
                            zvire.Popis = reader.GetString(7);
                        }
                        zvire.Datum_pridani = reader.GetDateTime(8);

                        listZvirat.Add(zvire);
                        
                    }
                }

                connection.Close();
                connection.Dispose();
            }

            return View(listZvirat);

        }

        public ActionResult VytvoritInzerat()
        {
            Zvire zvire = new Zvire();
            Obrazek obrazek = new Obrazek();
            ZvireObrazek zvireObrazek = new ZvireObrazek();
            zvireObrazek.Zvire = zvire;
            zvireObrazek.Obrazek = obrazek;

            return View("VytvoritInzerat", zvireObrazek);
        }

        public ActionResult VytvoritInzerat_form(Models.ZvireObrazek zvireObrazek)
        {


            /*
            using (MemoryStream memoryStream = new MemoryStream())
            {
                zvireObrazek.Obrazek.InputStream.CopyTo(memoryStream);
            }
            */


            string sql = $"INSERT INTO obrazek (id, datum_nahrani, datum_modifikace, obrazek)" +
                    $"VALUES (obrazekSequence.NextVal, sysdate, sysdate, '{zvireObrazek.Obrazek.Obrazek_soubor}')";

            using (OracleConnection connection = new OracleConnection(conString))
            {
                connection.Open();

                using (OracleCommand command = new OracleCommand(sql, connection))
                {
                    command.CommandText = sql;

                    command.ExecuteNonQuery();

                    connection.Close();
                    connection.Dispose();
                }
            }

            return View("Inzerat");

        }
/*
        public ActionResult FileInputObrazek()
        {

            Obrazek obrazek = new Obrazek();

            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.Filter = "Image files | *.jpg, *.png, *.bmp";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    String nazevSouboru = fileDialog.FileName;
                    obrazek.Obrazek_soubor = Image.FromFile(fileDialog.FileName);

                    /*

                    var virtualPath = string.Format("~/images/{0}.jpg", nazevSouboru);

                    if (System.IO.File.Exists(Server.MapPath(virtualPath)))
                    {
                        ViewBag.obrazek = VirtualPathUtility.ToAbsolute(virtualPath);
                    }
                    *


                    string sql = $"INSERT INTO uzivatel (id, uzivatelske_jmeno, heslo, email, datum_registrace)" +
                                 $"VALUES (obrazekSequence.NextVal, sysdate, sysdate, '{obrazek}', sysdate)";

                    using (OracleConnection connection = new OracleConnection(conString))
                    {
                        connection.Open();

                        using (OracleCommand command = new OracleCommand(sql, connection))
                        {
                            command.CommandText = sql;

                            command.ExecuteNonQuery();

                            connection.Close();
                            connection.Dispose();
                        }
                    }

                    return View("VytvoritInzerat");

                }
                else
                {
                    return View("Chyba");
                }

            }
            catch (Exception ex)
            {
                return View("Chyba");
            }

    
        }
        */
    }
}
