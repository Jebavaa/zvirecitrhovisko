﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace zvireci_trhovisko.Controllers
{

    public class UzivatelController : Controller
    {

        string conString = System.Configuration.ConfigurationManager.ConnectionStrings["orcl"].ConnectionString;


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PrihlasitSe()
        {
            return View("PrihlasitSe");
        }

        public ActionResult RegistrovatSe()
        {
            return View("RegistrovatSe");
        }

        public ActionResult Prihlasit_se_form(Models.Uzivatel uzivatel)
        {
            string sql = $"SELECT * FROM uzivatel WHERE uzivatelske_jmeno = '{uzivatel.Uzivatelske_jmeno}' AND heslo = '{uzivatel.Heslo}'";

            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = conString;
                connection.Open();

                OracleCommand command = new OracleCommand(sql, connection);

                using (OracleDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            int i = reader.GetInt32(0);
                            return View("Profil", uzivatel);
                        }
                        catch
                        {
                            return View("Chyba");
                        }
                    }

                    ViewBag.Zprava = "Nesprávné uživatelské jméno nebo heslo.";
                    return View("PrihlasitSe");
                }
            }
        }

        public ActionResult Registrovat_se_form(Models.Uzivatel uzivatel)
        {

            string sql = $"INSERT INTO uzivatel (id, uzivatelske_jmeno, heslo, email, datum_registrace)" +
                         $"VALUES (uzivatel_sequence.NextVal, '{uzivatel.Uzivatelske_jmeno}', '{uzivatel.Heslo}', '{uzivatel.Email}', sysdate)";


            try
            {
                using (OracleConnection connection = new OracleConnection(conString))
                {
                    connection.Open();

                    using (OracleCommand command = new OracleCommand(sql, connection))
                    {
                        command.CommandText = sql;

                        command.ExecuteNonQuery();

                        connection.Close();
                        connection.Dispose();
                    }
                }

                return View("Profil", uzivatel);
            }
            catch
            {
                return View("Chyba");
            }

            
        }

    }
}