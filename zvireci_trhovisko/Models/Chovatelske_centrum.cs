﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Chovatelske_centrum
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("Adresa")]
        public int Id_adresa { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Název nesmí být delší než 32 znaků.")]
        [Display(Name = "Název")]
        public string Nazev { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(255, ErrorMessage = "Text nemůže mít více jak 255 znaků.")]
        public string Popis { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 16 znaků.")]
        [Display(Name = "Telefonní číslo")]
        public int Tel { get; set; }

    }
}