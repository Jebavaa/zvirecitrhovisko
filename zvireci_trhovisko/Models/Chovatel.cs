﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Chovatel
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("Uzivatel")]
        public int Id_uzivatel { get; set; }

        [ForeignKey("Chovatelske_centrum")]
        public int? Id_chovatelske_centrum { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Text nesmí být delší než 32 znaků.")]
        [Display(Name = "Jméno")]
        public string Jmeno { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Text nesmí být delší než 32 znaků.")]
        [Display(Name = "Příjmení")]
        public string Prijmeni { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 16 znaků.")]
        [Display(Name = "Telefonní číslo")]
        public int Tel { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(255, ErrorMessage = "Text nemůže mít více jak 255 znaků.")]
        public string Popis { get; set; }

    }
}