﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Zvire
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Chovatel")]
        public int Id_chovatel { get; set; }

        [ForeignKey("Rozmer")]
        public int Id_rozmer { get; set; }

        [ForeignKey("Druh_zvirete")]
        public int Id_druh_zvirete { get; set; }

        [ForeignKey("Obrazek")]
        public int Id_obrazek { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Název nesmí být delší než 32 znaků.")]
        [Display(Name = "Název")]
        public string Nazev { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 12 znaků.")]
        public int Cena { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(255, ErrorMessage = "Text nemůže mít více jak 255 znaků.")]
        public string Popis { get; set; }

        public DateTime Datum_pridani { get; set; }


    }
}