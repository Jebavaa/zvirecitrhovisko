﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Druh_zvirete
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Text nesmí být delší než 32 znaků.")]
        public string Druh { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Text nesmí být delší než 32 znaků.")]
        public string Rod { get; set; }

    }
}