﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Rozmer
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 16 znaků.")]
        [Display(Name = "Výška (cm)")]
        public string Vyska_cm { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 16 znaků.")]
        [Display(Name = "Šířka (cm)")]
        public string Sirka_cm { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 16 znaků.")]
        [Display(Name = "Délka (cm)")]
        public string Delka_cm { get; set; }

    }
}