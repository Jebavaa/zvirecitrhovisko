﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class ZvireObrazek
    {
        public Zvire Zvire { get; set; }
        public Obrazek Obrazek { get; set; }
    }
}