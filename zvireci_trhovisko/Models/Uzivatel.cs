﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Uzivatel
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Uživatelské jméno nesmí být delší než 32 znaků.")]
        [Display(Name = "Uživatelské jméno")]
        public string Uzivatelske_jmeno { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Heslo nesmí být delší než 32 znaků", MinimumLength = 3)]
        public string Heslo { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Email nesmí být delší než 32 znaků a kratší než 5 znaků.", MinimumLength = 5)]
        public string Email { get; set; }

        public DateTime Datum_registrace { get; set; }

    }
}