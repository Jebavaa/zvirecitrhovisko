﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Adresa
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Text nesmí být delší než 32 znaků.")]
        public string Mesto { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 16 znaků.")]
        public int Psc { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Text nesmí být delší než 32 znaků.")]
        public string Ulice { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(16, ErrorMessage = "Text nesmí být delší než 16 znaků.")]
        public int Cp { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Text nesmí být delší než 32 znaků.")]
        public string Zeme { get; set; }

    }
}