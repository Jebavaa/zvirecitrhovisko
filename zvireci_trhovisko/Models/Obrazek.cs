﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;
using System.Drawing;

namespace zvireci_trhovisko.Models
{
    public class Obrazek
    {
        [Key]
        public int Id { get; set; }

        public DateTime Datum_nahrani { get; set; }

        public DateTime? Datum_modifikace { get; set; }

        public Image Obrazek_soubor { get; set; }

    }
}