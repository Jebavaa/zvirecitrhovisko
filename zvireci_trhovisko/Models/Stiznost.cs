﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Stiznost
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("Uzivatel")]
        public int? Id_uzivatel { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(255, ErrorMessage = "Text nemůže mít více jak 255 znaků.")]
        public string Popis { get; set; }

        public DateTime Datum_stiznosti { get; set; }

    }
}