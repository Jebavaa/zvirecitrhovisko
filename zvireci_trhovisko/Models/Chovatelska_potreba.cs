﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zvireci_trhovisko.Models
{
    public class Chovatelska_potreba
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("Rozmer")]
        public int Id_rozmer { get; set; }

        [ForeignKey("Obchod")]
        public int? Id_obchod { get; set; }

        [ForeignKey("Chovatel")]
        public int? Id_chovatel { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(32, ErrorMessage = "Název nesmí být delší než 32 znaků.")]
        [Display(Name = "Název")]
        public string Nazev { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné.")]
        [StringLength(12, ErrorMessage = "Text nesmí být delší než 12 znaků.")]
        public int Cena { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole nesmí být prázdné")]
        [StringLength(255, ErrorMessage = "Text nemůže mít více jak 255 znaků.")]
        public string Popis { get; set; }

    }
}